# Machine Learning ToolKit
Scripts aiming to help the development process and testing for machine learning algoritms.

### Authors

* Felipe de Azevedo Oliveira

### Requirements

* Docker

### Languages

* Python 3.6.9 or above

### Packages used and/or already configured

* Logging
* Pandas
* Numpy
* Seaborn
* Matplotlib
* Plotly
* Scikit
* Docopt
* Pyod
* Imblearn
* Dask

# Usage
This repository consists from mainly three scripts: 

* LoggerUtils.py - Script where log functions for warnings, information and erros are deployed;
* PreProcessUtils.py - Set of functions to help the pre-process step;
    * Isolated functions for:
        * Splitting dataframe in numerical and categorical values (`Data_Prep_Selector`);
        * Missing values handling (`Data_Prep_NA_values`). For numeric variables, the mean value will be added. For categorical values, a warning is given as result, but no action is taken;
        * Label Encoder (`Data_Prep_label_encoder`);
        * One Hot Encoder (`Data_Prep_One_Hot_Encoder`);
        * Standard Scaler (`Data_Prep_standard_scaler`);
        * Treatment of unbalanced data (`Data_Prep_Unbalanced`). The user can choose between the Tomek Links (`tomek`) or SMOTE(`smote`) methods;
        * Treating outliers (`Data_Prep_Outliers`). As a result, the function gives the index for each outlier detected. As default, the functions uses the KNN method on pyod;
        * Optimal feature selection (`Data_Prep_feature_selection`). Using ExtraTrees method to identify the relevance from each variable on dataset.
    * `Data_Prep` function. Automatically applies functions for a given dataframe: missing values (`Data_Prep_NA_values`), splitting dataframe in numerical and categorical values `Data_Prep_Selector`, Label Encoder (`Data_Prep_label_encoder`), One Hot Encoder (`Data_Prep_One_Hot_Encoder`), Standard Scaler (`Data_Prep_standard_scaler`).

* ClassificationUtils.py - Have functions to run the following machine learning models for classifications: Naive Bayes (naive_bayes), Decision Tree (decision_tree), Random Forest (random_forest), KNN (knn), Logistic Regression (logistic_regression), SVM (svm) and Neural Network (neural_network). The main objective is to train and test the algoritm using the cross-validation function (`cross_validation_eval`). Also, a function is provided, using GrindSearch to find the best parameters for a model (`GrindSearch_optmization`).

The dockerfile was written following [Hadolint](https://hub.docker.com/r/hadolint/hadolint) best practices.

Also, within all functions the function [dask.delayed](https://docs.dask.org/en/stable/delayed.html) was applied, highly improving each processing step. 

### Step 1
If you simply want to use this workflow, download and extract the latest release. 

If you intend to modify and further develop this workflow, fork this reposity. Please consider providing any generally applicable modifications via a pull request.

### Step 2
Add the script for the analysis and files inteded to be used on `/analysis`. By default, the container will search and run the python script named `analysis.py`. This is specified on file `run.sh`.

### Step 3
Build the image for Docker. Use the following command: `docker build -t ml_toolkit .`

# Example
On directory `example` is shown an example on Jupyter Notebook (`airlines.ipynb`) of a simple analysis using the Machine Learning Toolkit. The example dataset is already on directory, but can also be found on [Kaggle](https://www.kaggle.com/datasets/jimschacko/airlines-dataset-to-predict-a-delay).
