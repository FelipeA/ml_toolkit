from utils.LoggerUtils import config_logger, message_debug, message_info, message_warning
from dask import delayed

def Data_Prep(pdData):
    import numpy as np
    pdData = Data_Prep_NA_values(pdData)
    numerical_columns,categorical_columns = Data_Prep_Selector(pdData)
    if numerical_columns and categorical_columns:
        message_info("Found both categorical and numerical values. Spliting dataframe")
        pdData_numerical = pdData[numerical_columns]
        pdData_categorical = pdData[categorical_columns]
        numpy_array_categorical = pdData_categorical.values
        numpy_array_numerical = pdData_numerical.values
        message_info("Calling label encoder function")
        numpy_array_categorical = Data_Prep_label_encoder(numpy_array_categorical)
        message_info("Calling One Hot Encoder function")
        numpy_array_categorical = Data_Prep_One_Hot_Encoder(numpy_array_categorical)
        message_info("Calling Standard Scaler function")
        numpy_array_numerical = Data_Prep_standard_scaler(numpy_array_numerical)
        message_info("Concatanating arrays")
        concat_array = np.concatenate([numpy_array_categorical,numpy_array_numerical],axis=1)
        return concat_array
    elif numerical_columns and not categorical_columns:
        message_info("Only Numeric type of variables were detected")
        numpy_array_numerical = pdData.values
        numpy_array_numerical = Data_Prep_standard_scaler(numpy_array_numerical)
        return numpy_array_numerical
    elif categorical_columns and not numerical_columns:
        message_info("Only Categorical type of variables were detected")
        numpy_array_categorical = pdData.values
        message_info("Calling label encoder function")
        numpy_array_categorical = Data_Prep_label_encoder(numpy_array_categorical)
        message_info("Calling One Hot Encoder function")
        numpy_array_categorical = Data_Prep_One_Hot_Encoder(numpy_array_categorical)
        return numpy_array_categorical

def Data_Prep_Selector(pdData):
    from sklearn.compose import make_column_selector as selector
    message_info(f"Initiating Selector function to separate categorical and numerical variables.")
    numerical_columns_selector = selector(dtype_exclude=object)
    categorical_columns_selector = selector(dtype_include=object)
    numerical_columns = numerical_columns_selector(pdData)
    categorical_columns = categorical_columns_selector(pdData)
    message_info("Separated types of variables")
    return numerical_columns,categorical_columns

def Data_Prep_NA_values(data):
    n_col = len(data.columns)
    for n in range(n_col):
        colName = data.columns[n]
        NullNumber = data[colName].isnull().sum()
        if NullNumber > 0:
            try:
                data[colName].fillna(data[colName].mean(), inplace = True)
                message_info(f"Detected missing values on the following column. Added Mean value: {data.columns[n]}")
            except:
                message_warning(f"Detected missing values for column ({data.columns[n]}). Categorical values detected.Column maintened")
        else:
            message_info("Not detected missing values in column.")
    message_info("Missing values checked sucessfully.")
    return data

def Data_Prep_label_encoder(ArrayData):
    from sklearn.preprocessing import LabelEncoder
    message_info("Aplying label encoder function...")
    label_encoder_function = LabelEncoder()
    try:
        n_col = ArrayData.shape[1]
    except:
        message_warning("Reshape needed for numpy array.")
        ArrayData=ArrayData.reshape(-1,1)
        n_col = ArrayData.shape[1]
    for n in range(n_col):
        ArrayData[:,n] = delayed(label_encoder_function.fit_transform)(ArrayData[:,n]).compute()
    return ArrayData

def Data_Prep_One_Hot_Encoder(ArrayData):
    from sklearn.preprocessing import OneHotEncoder
    import numpy as np
    message_info("Initiating OneHotEncoder")
    onehot = OneHotEncoder(handle_unknown='ignore')
    ArrayData = delayed(onehot.fit_transform)(ArrayData).compute()
    ArrayData = ArrayData.toarray()
    return ArrayData

def Data_Prep_standard_scaler(data):
  message_info("Processing variables with standard scaler...")
  from sklearn.preprocessing import StandardScaler
  scaler = StandardScaler()
  data = delayed(scaler.fit_transform)(data).compute()
  message_info("Standard Scaler process finished!")
  return(data)

def Data_Prep_Unbalanced(x_data,y_data,type):
    if type=="tomek":
        message_info("Initiating Tomek Links downsampling function...")
        from imblearn.under_sampling import TomekLinks
        tl = TomekLinks(sampling_strategy='all')
        x_under, y_under = delayed(tl.fit_resample)(x_data, y_data).compute()
        message_info("Tomek Links finalized!")
        return x_under,y_under
    elif type=="smote":
        message_info("Initiating SMOTE oversampling function...")
        from imblearn.over_sampling import SMOTE
        smote = SMOTE(sampling_strategy='minority')
        x_over, y_over = delayed(smote.fit_resample)(x_data, y_data).compute()
        message_info("SMOTE finalized!")
        return x_over, y_over

def Data_Prep_Outliers(x_data):
    from pyod.models.knn import KNN
    detector = KNN()
    delayed(detector.fit)(x_data).compute()
    prev = detector.labels_
    outliers = []
    for i in range(len(prev)):
        if prev[i] == 1:
            outliers.append(i)
    return outliers

def Data_Prep_feature_selection(x_data,y_data):
    from sklearn.ensemble import ExtraTreesClassifier
    selection = ExtraTreesClassifier()
    delayed(selection.fit(x_data,y_data)).compute
    relevance = selection.feature_importances_
    return relevance
