from utils.LoggerUtils import config_logger, message_debug, message_info, message_warning
from dask import delayed
# Classification Functions

## Naive Bayes
def Data_Naive_Bayes():
  from sklearn.naive_bayes import GaussianNB
  naive_model = GaussianNB()
  return naive_model

## Decision tree
def Data_decision_tree(best_params):
  from sklearn.tree import DecisionTreeClassifier
  tree = DecisionTreeClassifier()
  tree.set_params(**best_params)
  return tree

## Random forest
def Data_random_forest(best_params):
  from sklearn.ensemble import RandomForestClassifier
  random_forest = RandomForestClassifier()
  random_forest.set_params(**best_params)
  return random_forest

## KNN
def Data_knn(best_params):
  from sklearn.neighbors import KNeighborsClassifier
  knn = KNeighborsClassifier()
  knn.set_params(**best_params)
  return knn

## Logistic Regression
def Data_logistic_regression(best_params):
  from sklearn.linear_model import LogisticRegression
  logistic = LogisticRegression()
  logistic.set_params(**best_params)
  return logistic

## SVM
def Data_svm(best_params):
  from sklearn.svm import SVC
  svm = SVC()
  svm.set_params(**best_params)
  return svm

## Neural Network
def Data_neural_network(best_params):
  from sklearn.neural_network import MLPClassifier
  neural = MLPClassifier()
  neural.set_params(**best_params)
  return neural
  
# Evaluation Functions
## Cross Validation
def cross_validation_eval(model,n_iter,k_fold_number,x_data,y_data,best_params):
  from sklearn.model_selection import cross_val_score, KFold
  message_info("Cross-validation evaluation initiated...")
  result = []
  for i in range(n_iter):
    message_info(f"Cross-validation loop: {i}")
    kfold = KFold(n_splits=int(k_fold_number), shuffle=True, random_state=i)
    if model == "naive_bayes":
      build_model = Data_Naive_Bayes()
      scores = delayed(cross_val_score)(build_model,x_data,y_data,cv=kfold).compute()
    elif model == "decision_tree":
      build_model = Data_decision_tree(best_params)
      scores = delayed(cross_val_score)(build_model,x_data,y_data,cv=kfold).compute()
    elif model == "random_forest":
      build_model = Data_random_forest(best_params)
      scores = delayed(cross_val_score)(build_model,x_data,y_data,cv=kfold).compute()
    elif model == "knn":
      build_model = Data_knn(best_params)
      scores = delayed(cross_val_score)(build_model,x_data,y_data,cv=kfold).compute()
    elif model == "logistic_regression":
      build_model = Data_logistic_regression(best_params)
      scores = delayed(cross_val_score)(build_model,x_data,y_data,cv=kfold).compute()
    elif model == "svm":
      build_model = Data_svm(best_params)
      scores = delayed(cross_val_score)(build_model,x_data,y_data,cv=kfold).compute()
    elif model == "neural_network":
      build_model = Data_neural_network(best_params)
      scores = delayed(cross_val_score)(build_model,x_data,y_data,cv=kfold).compute()
    result.append(scores.mean())
  message_info("Cross-validation finished!")
  return result,build_model


# Finding possible best parameters for models
def GrindSearch_optmization(model,list_parameters,x_data,y_data):
  message_info("Checking for optimal parameters...")   
  from sklearn.model_selection import GridSearchCV
  if model == "decision_tree":
    from sklearn.tree import DecisionTreeClassifier
    grid_search = delayed(GridSearchCV)(estimator=DecisionTreeClassifier(), param_grid=list_parameters).compute()
  elif model == "random_forest":
    from sklearn.ensemble import RandomForestClassifier
    grid_search = delayed(GridSearchCV)(estimator=RandomForestClassifier(), param_grid=list_parameters).compute()
  elif model == "knn":
    from sklearn.neighbors import KNeighborsClassifier
    grid_search = delayed(GridSearchCV)(estimator=KNeighborsClassifier(), param_grid=list_parameters).compute()
  elif model == "logistic_regression":
    from sklearn.linear_model import LogisticRegression
    grid_search = delayed(GridSearchCV)(estimator=LogisticRegression(), param_grid=list_parameters).compute()
  elif model == "svm":
    from sklearn.svm import SVC
    grid_search = delayed(GridSearchCV)(estimator=SVC(), param_grid=list_parameters).compute()
  elif model == "neural_network":
    from sklearn.neural_network import MLPClassifier
    grid_search = delayed(GridSearchCV)(estimator=MLPClassifier(), param_grid=list_parameters).compute()
  delayed(grid_search.fit(x_data,y_data)).compute()
  best_params = grid_search.best_params_
  best_results = grid_search.best_score_
  message_info(f"Optimal parameters found sucessfully: {best_params,best_results}")
  return best_params,best_results