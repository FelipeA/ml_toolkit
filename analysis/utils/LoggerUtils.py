import logging
import sys

def config_logger(log_level: str) -> None:
    root = logging.getLogger('ML_toolkit')
    root.setLevel(log_level)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(log_level)
    formatter = logging.Formatter(
        ('%(asctime)s ' '- %(name)s ' '- %(levelname)s ' '- %(message)s')
    )
    handler.setFormatter(formatter)
    root.addHandler(handler)

def message_debug(message: str) -> None:
    return logging.getLogger('ML_toolkit').debug(message)


def message_info(message: str) -> None:
    return logging.getLogger('ML_toolkit').info(message)


def message_warning(message: str) -> None:
    return logging.getLogger('ML_toolkit').warning(message)


def message_error(message: str) -> None:
    return logging.getLogger('ML_toolkit').error(message)


def message_critical(message: str) -> None:
    return logging.getLogger('ML_toolkit').critical(message)


def LOG(func):
    def wrapper(*args, **kwargs):
        message_debug(
            f'call function: {func.__name__}{args} with parameters {kwargs}'
        )
        value = func(*args, **kwargs)
        message_debug(
            f'function: {func.__name__}{args} returned values: {value}'
        )
        return value

    return wrapper