FROM ubuntu:22.04

RUN apt-get update && apt-get install --no-install-recommends -y \
    python3-pip=22.0.2+dfsg-1 \
    wget=1.21.2-2ubuntu1 \
    && pip install --no-cache-dir docopt==0.6.2 \
    && pip install --no-cache-dir scikit-learn==1.1.1 \
    && pip install --no-cache-dir yellowbrick==1.4 \
    && pip install --no-cache-dir pandas==1.4.3 \
    && pip install --no-cache-dir seaborn==0.11.2 \
    && pip install --no-cache-dir plotly==5.9.0 \
    && pip install --no-cache-dir pyod==1.0.3 \
    && pip install --no-cache-dir imblearn==0.0 \
    && pip install --no-cache-dir dask==2022.7.1 \
    && rm -rf var/lib/apt/list/*

COPY ./run.sh /usr/local/bin
RUN chmod +x /usr/local/bin/run.sh

ENTRYPOINT ["/usr/local/bin/run.sh"]
